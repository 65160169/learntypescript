let mytupple: [number,boolean,string];
mytupple = [5,false,"this is mytupple"];
mytupple.push(1);
console.log(mytupple); 
//แบบนี้ยัง push ได้อยู่

let mytupple2:readonly [number,boolean,string];
mytupple2 = [5,false,"this is mytupple"];
console.log(mytupple2); 
//แบบนี้จะ push ไม่ได้เพราะว่าใส่ readonly