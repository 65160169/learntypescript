let json:any = JSON.parse("55"); //any คือตัวแปรนี้ใส่อะไรก็ได้เป็นได้ทั้ง string และ number
console.log(typeof json);
json = "55";
console.log(typeof json);

//ที่มันเป็น type number เพราะว่าใช้ JSON.parse คือการแปลงจาก string เป็น JSON และ
//JSON จะเเปลงเป็น type ที่เหมาะสมกับค่าของตัวแปรนั้นมากที่สุด