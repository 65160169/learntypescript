type Caryear = number;
type Cartype = string;
type Carmodel = string;

type car = {
    year: Caryear,
    type: Cartype,
    model: Carmodel
}

const carYear:Caryear = 2001;
const carType:Cartype = "Toyota"
const carModel:Carmodel = "Corolla"

const car1: car = {
    year: 2001,
    type: "Nissan",
    model: "GTR"
}

console.log(car1)